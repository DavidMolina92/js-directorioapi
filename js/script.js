
const apiURL = "https://gateway.marvel.com:443/v1/public/characters?ts=1&apikey=d9211e26d6002984e4d1c8a473234638&hash=e6c88e3c2a20ae740c65c7e0128d6024";


window.onload = function() {
    init();
}

let marvelCharacters;

const init = async() => {
    marvelCharacters = await getCharacters();
    marvelCharacters = marvelCharacters.data.results;
    mappedCharacters(marvelCharacters);
}

const getCharacters = async() => {
    const result = await fetch(apiURL);
    const resultToJson = await result.json();
    return resultToJson;
}

const mappedCharacters = (marvelCharacters) => {
    marvelCharacters.map((character) => { 
        return printChracters({ 
            name: character.name, 
            img: character.thumbnail, 
            description: character.description, 
            modified: character.modified
        });
    });
}

const printChracters = (character) => {
    let characterContainer = document.querySelector("#hero__container");
    if (character.description == "") {
        character.description = "No hay descripción.";
    }

    characterContainer.innerHTML += ` 
        <div class="b-list__column">
            <div class="b-list__hero">
                <div class="b-list__image">
                    <img src=${character.img.path}.${character.img.extension} alt=${character.name}>
                </div>
                <div class="b-list__text" id="b-list__text">
                    <p><span class="b-list__text--bold">Nombre: </span>${character.name}</p>
                    <p><span class="b-list__text--bold">Descripción: </span>${character.description}</p>
                    <p><span class="b-list__text--bold">Fecha de modificación: </span>${character.modified}</p>
                </div>
            </div>   
        </div>
        `;
}

const addEvents = () => {
    const $$button = document.querySelector('#b-search__button');
    $$button.addEventListener("click", inputInfoSearch);
}

const inputInfoSearch = () => {
    deleteResults();
    const $$input = document.querySelector("#b-search__input").value;
    const pattern = $$input.toUpperCase();
    let names = filterByName(pattern);
    mappedCharacters(names);   
}

const filterByName = (pattern) => {
        let resultsContainer = marvelCharacters.filter( (character)=> {
        let hero = character.name.toUpperCase();
        return hero.includes(pattern);
    });   
    return resultsContainer;
}

function deleteResults() {
    let deleteResult = document.querySelector("#hero__container");
    deleteResult.remove();
    let addResult = document.querySelector("#b-list");
    addResult.innerHTML += `<div class="b-list__row" id="hero__container"></div>`;
}
